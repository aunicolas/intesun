# Interactive sunbursts

[EC3](https://aunicolas.gitlab.io/intesun/EC3.html)

[EC4](https://aunicolas.gitlab.io/intesun/EC4.html)

[EC8](https://aunicolas.gitlab.io/intesun/EC8.html)

[EC24](https://aunicolas.gitlab.io/intesun/EC24.html)

[SC3](https://aunicolas.gitlab.io/intesun/SC3.html)

[SC34](https://aunicolas.gitlab.io/intesun/SC34.html)

[SC38](https://aunicolas.gitlab.io/intesun/SC38.html)

[SC324](https://aunicolas.gitlab.io/intesun/SC324.html)

[SE3](https://aunicolas.gitlab.io/intesun/SE3.html)

[SE324](https://aunicolas.gitlab.io/intesun/SE324.html)
